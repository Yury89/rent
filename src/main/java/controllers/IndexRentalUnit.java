package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import services.RentalUnitService;

@Controller
@RequestMapping(value = "/")
public class IndexRentalUnit {

    @Autowired
    private RentalUnitService rentalUnitService;


    @RequestMapping
    public String mainRentalUnits(Model model) {
        model.addAttribute("mainRentalUnits", rentalUnitService.getMainRentalUnits());
        return "index";
    }

}
