package controllers;

import models.RentalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import services.RentalUnitService;
@Controller
@RequestMapping(value = "/addRentalUnit")
public class AddRentalUnit  {


    @Autowired
    private RentalUnitService rentalUnitService;


    @RequestMapping(method = RequestMethod.GET)
    public String addRentalUnitForm(Model model) {
        model.addAttribute("rentalForm",new RentalUnit());
        model.addAttribute("allRentalUnits",rentalUnitService.getAllRentalUnit());
        return "addRentalUnit";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String saveRentalUnitForm(@ModelAttribute("rentalForm") RentalUnit rentalUnit, @RequestParam(name = "nameForRentalUnit")String idForRentalUnit){
        rentalUnitService.saveImageAndRentalUnit(rentalUnit,idForRentalUnit);

        return "redirect:/";
    }


}
