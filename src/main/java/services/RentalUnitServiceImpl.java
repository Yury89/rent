package services;

import models.Amenities;
import models.Polices;
import models.RentalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.RentalUnitRepositoryImpl;

import java.util.Iterator;
import java.util.LinkedHashSet;

@Service
public class RentalUnitServiceImpl extends RentalUnitRepositoryImpl implements RentalUnitService {

    @Autowired
    private ImageService imageService;

    @Override
    public LinkedHashSet<RentalUnit> getParentRentalUnitsById(String id) {
        RentalUnit parentRentalUnitById = getById(id);
        LinkedHashSet<RentalUnit> rentalUnits = new LinkedHashSet<RentalUnit>();
        while (parentRentalUnitById.getRentalUnit() != null) {
            parentRentalUnitById = parentRentalUnitById.getRentalUnit();
            rentalUnits.add(parentRentalUnitById);
        }
        return rentalUnits;
    }


    @Override
    public LinkedHashSet<Amenities> getAllAmenities(LinkedHashSet<RentalUnit> rentalUnits) {
        Iterator<RentalUnit> iterator = rentalUnits.iterator();
        LinkedHashSet<Amenities> amenities = new LinkedHashSet<Amenities>();
        while (iterator.hasNext())
            amenities.addAll(iterator.next().getAmenities());
        return amenities;
    }


    @Override
    public LinkedHashSet<Polices> getAllPolices(LinkedHashSet<RentalUnit> rentalUnits) {
        Iterator<RentalUnit> iterator = rentalUnits.iterator();
        LinkedHashSet<Polices> amenities = new LinkedHashSet<Polices>();

        while (iterator.hasNext())
            amenities.addAll(iterator.next().getPolices());
        return amenities;
    }

    @Override
    public void saveImageAndRentalUnit(RentalUnit rentalUnit, String id) {
        if (!id.equals("-1"))
            rentalUnit.setRentalUnit(super.getById(id));
        saveProperty(rentalUnit);
        for (int i = 0; i < rentalUnit.getImages().size(); i++) {
            imageService.saveImage(rentalUnit.getImages().get(i),rentalUnit.getName() + rentalUnit.getTitle() + i);
        }
        saveRentalUnit(rentalUnit);

    }
}
