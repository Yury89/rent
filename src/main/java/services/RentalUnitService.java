package services;

import models.Amenities;
import models.Polices;
import models.RentalUnit;
import repositories.RentalUnitRepository;

import java.util.LinkedHashSet;

public interface RentalUnitService extends RentalUnitRepository{


    LinkedHashSet<RentalUnit> getParentRentalUnitsById(String id);

    LinkedHashSet<Amenities> getAllAmenities(LinkedHashSet<RentalUnit> rentalUnits);

    LinkedHashSet<Polices> getAllPolices(LinkedHashSet<RentalUnit> rentalUnits);

    void saveImageAndRentalUnit(RentalUnit rentalUnit, String id);

}
