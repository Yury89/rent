package services;

import com.mongodb.gridfs.GridFSDBFile;
import models.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import repositories.ImageRepository;

import java.io.IOException;

@Service
public class ImageService {

    @Autowired
    private GridFsTemplate gridFsTemplate;

    @Autowired
    private ImageRepository imageRepository;

    public void saveImage(Image image,String name){
        image.setName(name);
        try {
            gridFsTemplate.store(image.getMultipartFile().getInputStream(),name,image.getMultipartFile().getContentType());

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageRepository.saveImage(image);
    }

    public GridFSDBFile getImage(String name){

       return gridFsTemplate.findOne(new Query().addCriteria(Criteria.where("filename").is(name)));

    }
}
