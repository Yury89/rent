package models;


import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class Polices {
    @Id
    private String id;
    @Field
    private String name;
    @Field
    private String value;

    @PersistenceConstructor
    public Polices(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @PersistenceConstructor
    public Polices() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Polices{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
