package models;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class Amenities {

    @Id
    private String id;
    @Field
    private String title;
    @Field
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @PersistenceConstructor
    public Amenities(String title, String description) {

        this.title = title;
        this.description = description;
    }

    @PersistenceConstructor
    public Amenities() {
    }

    @Override
    public String toString() {
        return "Amenities{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
