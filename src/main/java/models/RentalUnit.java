package models;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Document
public class RentalUnit {

    @Id
    private String id;
    @Field
    private String name;
    @Field
    private String title;
    @Field
    private String description;
    @DBRef
    private List<Image> images;
    @DBRef
    private Price price;
    @DBRef
    private Address address;
    @DBRef
    private List<Amenities> amenities;
    @DBRef
    private List<Polices> polices;
    @DBRef
    private RentalUnit rentalUnit;

    @PersistenceConstructor
    public RentalUnit(String name, String title, String description, Price price, Address address, List<Amenities> amenities, List<Polices> polices,RentalUnit rentalUnit,List<Image> images) {
        this.name = name;
        this.title = title;
        this.description = description;
        this.price = price;
        this.address = address;
        this.amenities = amenities;
        this.polices = polices;
        this.rentalUnit = rentalUnit;
        this.images = images;



    }


    public RentalUnit() {
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public RentalUnit getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Amenities> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Amenities> amenities) {
        this.amenities = amenities;
    }

    public List<Polices> getPolices() {
        return polices;
    }

    public void setPolices(List<Polices> polices) {
        this.polices = polices;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "RentalUnit{" +
                "name='" + name + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", address=" + address +
                ", amenities=" + amenities +
                ", polices=" + polices +
                '}';
    }
}