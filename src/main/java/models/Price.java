package models;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigDecimal;

@Document
public class Price {
    @Id
    private String id;
    @Field
    private BigDecimal day;
    @Field
    private BigDecimal week;
    @Field
    private BigDecimal month;

    @PersistenceConstructor
    public Price() {
    }

    @PersistenceConstructor
    public Price(BigDecimal day, BigDecimal week, BigDecimal month) {
        this.day = day;
        this.week = week;
        this.month = month;
    }

    public BigDecimal getDay() {
        return day;
    }

    public void setDay(BigDecimal day) {
        this.day = day;
    }

    public BigDecimal getWeek() {
        return week;
    }

    public void setWeek(BigDecimal week) {
        this.week = week;
    }

    public BigDecimal getMonth() {
        return month;
    }

    public void setMonth(BigDecimal month) {
        this.month = month;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Price{" +
                "day=" + day +
                ", week=" + week +
                ", month=" + month +
                '}';
    }
}
