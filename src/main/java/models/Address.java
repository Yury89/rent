package models;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class Address  {

    @Id
    private String id;
    @Field
    private String country;
    @Field
    private String region;
    @Field
    private String city;
    @Field
    private String street;
    @Field
    private String homeNumber;
    @Field
    private String room;

    @PersistenceConstructor
    public Address() {
    }

    @PersistenceConstructor
    public Address(String country, String region, String city, String street, String homeNumber, String room) {
        this.country = country;
        this.region = region;
        this.city = city;
        this.street = street;
        this.homeNumber = homeNumber;
        this.room = room;
    }

    public String getCountry() {

        return country;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return "Address{" +
                "country='" + country + '\'' +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", homeNumber='" + homeNumber + '\'' +
                ", room='" + room + '\'' +
                '}';
    }
}
