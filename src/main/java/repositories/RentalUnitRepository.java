package repositories;

import models.RentalUnit;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RentalUnitRepository {

    List<RentalUnit> getMainRentalUnits();

    List<RentalUnit> getChildrenRentalUnitsById(String id);

    RentalUnit getById(String id);

    List<RentalUnit> getAllRentalUnit();

    void saveProperty(RentalUnit rentalUnit);

    void saveRentalUnit(RentalUnit rentalUnit);

}
