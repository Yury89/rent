package repositories;

import models.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

@Repository
public class ImageRepository {

    @Autowired
    private MongoOperations mongoOperations;

    public void saveImage(Image image){
        mongoOperations.save(image);
    }
}
