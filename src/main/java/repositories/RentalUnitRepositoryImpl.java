package repositories;

import models.Amenities;
import models.Polices;
import models.RentalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RentalUnitRepositoryImpl implements RentalUnitRepository {

    @Autowired
    private MongoOperations mongoOperations;

    @Override
    public List<RentalUnit> getMainRentalUnits() {
        Query query = new Query();
        query.addCriteria(Criteria.where("rentalUnit").is(null));
        return mongoOperations.find(query, RentalUnit.class);
    }

    @Override
    public List<RentalUnit> getChildrenRentalUnitsById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("rentalUnit").is(id));
        return mongoOperations.find(query, RentalUnit.class);
    }

    @Override
    public RentalUnit getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return mongoOperations.findOne(query, RentalUnit.class);
    }

    @Override
    public List<RentalUnit> getAllRentalUnit() {
        return mongoOperations.findAll(RentalUnit.class);
    }

    @Override
    public void saveProperty(RentalUnit rentalUnit) {

        mongoOperations.save(rentalUnit.getAddress());
        mongoOperations.save(rentalUnit.getPrice());
        for (Amenities amenities : rentalUnit.getAmenities()) {
            mongoOperations.save(amenities);
        }
        for (Polices polices : rentalUnit.getPolices()) {
            mongoOperations.save(polices);
        }
    }

    @Override
    public void saveRentalUnit(RentalUnit rentalUnit) {
        mongoOperations.save(rentalUnit);
    }
}
