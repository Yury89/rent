<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<t:template>
    <div class="row">
        <div class=" col l6 offset-l2">
            <ul class="pagination">

                <c:forEach items="${parent}" var="par">
                    <li class="waves-effect"><a href="<c:url value="/rentalunit?id=${par.id}"/>"><h5
                            class="blue-text">${par.title}</h5></a></li>
                    <li><i class="material-icons tiny">trending_flat</i></li>
                </c:forEach>
                 <li class="disabled"><a href="<c:url value="/rentalunit?id=${current.id}"/>"><h5
                    class="materialize-red-text">${current.title}</h5></a></li>
                <c:if test="${child.size() != 0}">
                    <li><i class="material-icons tiny">trending_flat</i></li>
                </c:if>
                <c:forEach items="${child}" var="childs" varStatus="step">
                    <li class="waves-effect"><a href="<c:url value="/rentalunit?id=${childs.id}"/>"><h5
                            class="blue-text">${childs.title}</h5></a></li>
                    <c:if test="${step.count != child.size()}">
                        <li><i class="material-icons tiny">trending_flat</i></li>
                    </c:if>
                </c:forEach>
            </ul>
        </div>
    </div>
    <c:if test="${current.images.size() != 0}">
    <div class="slider">
        <ul class="slides">
            <c:forEach items="${current.images}" var="image">
                <li>
                    <img class="activator cursor-pointer"
                         src="/uploaded_images/?name=${image.name}">


                    <div class="caption center-align">
                    </div>
                </li>
            </c:forEach>

        </ul>
    </div>
        <div class="progress">
            <div class="determinate" style="width: 100%"></div>
        </div>
    </c:if>

    <div class="row">

        <div class="col s4 z-depth-2">
            <c:if test="${current.price != null}">
                <h2>Prices: </h2>
            </c:if>
            <div class="col offset-l1">

                    <h3>For Day: ${current.price.month}</h3>
                    <br/>
                <h3>For Month: ${current.price.month}</h3>
                <br/>
                <h3>For Week: ${current.price.week}</h3>
                <br/>
                <c:forEach items="${polices}" var="pol">
                    <h3>${pol.name}: ${pol.value}</h3>
                    <br/>
                </c:forEach>
            </div>
            <c:if test="${current.amenities.size() != 0 || amenities.size() != 0}">
            <h2>Amenities: </h2>
            </c:if>
            <div class="col offset-l1">
            <c:forEach items="${current.amenities}" var="amen">
                <h3>${amen.title}: ${amen.description}</h3>
                <br/>
            </c:forEach>
            <c:forEach items="${amenities}" var="amen">
                <h3>${amen.title}: ${amen.description}</h3>
                <br/>
            </c:forEach>
            </div>
            <c:if test="${polices.size() != 0 || current.polices.size() != 0}">
            <h2>Polices: </h2>
            </c:if>
            <div class="col offset-l1">
            <c:forEach items="${current.polices}" var="pol">
                <h3>${pol.name}: ${pol.value}</h3>
                <br/>
            </c:forEach>
            <c:forEach items="${polices}" var="pol">
                <h3>${pol.name}: ${pol.value}</h3>
                <br/>
            </c:forEach>
                </div>




        </div>

        <div class="col s8">
            <div class="row">
                <c:forEach items="${child}" var="rent">
                    <div class=" card col s6 m12 l4 card-panel hoverable">
                        <div class="card-image">

                            <c:if test="${rent.images.size() != 0}">

                                <img class="activator cursor-pointer"
                                     src="/uploaded_images/?name=${rent.images.get(0).name}">
                            </c:if>
                            <c:if test="${rent.images.size() == 0}">
                                <img class="activator cursor-pointer"
                                     src="<c:url  value="/resources/velo1.jpg"/>">
                            </c:if>


                            <span class="card-title">${rent.title}</span>

                        </div>
                        <div class="card-content">
                            <p>${rent.description}</p>

                        </div>
                        <div class="card-action">
                            <a href="<c:url value="/rentalunit?id=${rent.id}"/>">More</a>
                        </div>
                        <div class="card-reveal accent-2">
                <span class="card-title grey-text text-darken-4 "><a href="<c:url value="/rentalunit?id=${rent.id}"/>">${rent.title}</a><i
                        class="material-icons right">close</i></span>

                            <div class="row">
                                <c:if test="${rent.price != null}">
                                    <div class="card ">
                                        <div class="card-content black-text">
                                            <span class="card-title">Price List</span>
                                            <p> <c:if test="${rent.price.month != null}">
                                                For Month ${rent.price.month}
                                            </c:if>
                                                <br>
                                                <c:if test="${rent.price.week != null}">
                                                    For Week ${rent.price.week}
                                                </c:if>
                                                <br>
                                                <c:if test="${rent.price.day != null}">
                                                    For Day ${rent.price.day}
                                                </c:if>
                                                <br>
                                            </p>
                                        </div>
                                    </div>
                                </c:if>

                                <c:if test="${rent.address != null}">
                                    <div class="card ">
                                        <div class="card-content black-text">
                                            <span class="card-title">Address</span>
                                            <p>${rent.address.country} ${rent.address.region} ${rent.address.city}
                                                    ${rent.address.street} ${rent.address.homeNumber} ${rent.address.room}</p>
                                        </div>
                                    </div>
                                </c:if>

                                <c:if test="${rent.polices.size() != 0}">
                                    <div class="card ">
                                        <div class="card-content black-text">
                                            <span class="card-title">Polices</span>
                                            <p><c:forEach items="${rent.polices}" var="polic">
                                                ${polic.name}: ${polic.value} <br>
                                            </c:forEach>
                                            </p>
                                        </div>
                                    </div>
                                </c:if>
                            </div>

                        </div>
                    </div>
                </c:forEach>
            </div>

        </div>
    </div>
    <script>


    </script>
</t:template>