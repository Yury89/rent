<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:template>
    <div class="row">
        <div class="input-field col s8">
            <div class="row">

                <c:url var="action" value="/addRentalUnit"/>
                <form:form cssClass="col s12" method="post" action="${action}" modelAttribute="rentalForm"
                           enctype="multipart/form-data">
                    <div class="row">
                        <div class="input-field col s6">
                            <form:input path="name" id="name" placeholder="Enter Name"/>
                            <form:label path="name">Name</form:label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <form:input path="title" id="title" placeholder="Enter Title"/>
                            <form:label path="title">Title</form:label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <form:input path="description" id="description" placeholder="Enter Description"/>
                            <form:label path="description">Description</form:label>
                        </div>
                    </div>
                    <div class="row">
                        <h3>Prices:</h3>
                        <div class="input-field col offset-l3 s6">
                            <form:input path="price.day" id="price_day" placeholder="Enter Price for Day"/>
                            <form:label path="price.day">Price For Day</form:label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:input path="price.week" id="price_week" placeholder="Enter Price for Week"/>
                            <form:label path="price.week">Price For Week</form:label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:input path="price.month" id="price_month" placeholder="Enter Price for Month"/>
                            <form:label path="price.month">Price For Month</form:label>
                        </div>
                    </div>
                    <div class="row">
                        <h3>Adress:</h3>
                        <div class="input-field col offset-l3 s6">
                            <form:input path="address.country" id="address_coutry" placeholder="Enter Address Country"/>
                            <form:label path="address.country">Address Country</form:label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:input path="address.region" id="address_region" placeholder="Enter Address Region"/>
                            <form:label path="address.region">Price Address Region</form:label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:input path="address.city" id="address_city" placeholder="Enter Address City"/>
                            <form:label path="address.city">Enter Address City</form:label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:input path="address.street" id="address_street" placeholder="Enter Address Street"/>
                            <form:label path="address.street">Enter Address Street</form:label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:input path="address.homeNumber" id="adress_homenumber"
                                        placeholder="Enter Adress Home Number"/>
                            <form:label path="address.homeNumber">Enter Address Home Number</form:label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:input path="address.room" id="adress_room" placeholder="Enter Number Room"/>
                            <form:label path="address.room">Enter Number Room</form:label>
                        </div>
                    </div>
                    <h3>Amenities</h3>
                    <input type="button" value="Add another text input" class="waves-effect waves-light btn"
                           onClick="addInput('addAmenityForm');">
                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:input path="amenities[0].title" placeholder="Enter Amenities Title 0"/>
                            <form:label path="amenities[0].title">Amenities Title 0</form:label>
                        </div>
                    </div>

                    <div class="row" id="addAmenityForm">
                        <div class="input-field col offset-l3 s6">
                            <form:input path="amenities[0].description" placeholder="Enter Amenities description 0"/>
                            <form:label path="amenities[0].description">Amenities description 0</form:label>
                        </div>
                    </div>
                    <h3>Polices</h3>
                    <input type="button" value="Add another text input" class="waves-effect waves-light btn"
                           onClick="addInput('addPolices');">
                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:input path="polices[0].name" placeholder="Enter Polices Name 0"/>
                            <form:label path="polices[0].name">Polices Name 0</form:label>
                        </div>
                    </div>

                    <div class="row" id="addPolices">
                        <div class="input-field col offset-l3 s6">
                            <form:input path="polices[0].value" placeholder="Enter Polices Value 0"/>
                            <form:label path="polices[0].value">Polices Value 0</form:label>
                        </div>
                    </div>

                    <h3>Images</h3>
                    <input type="button" value="Add another file input" class="waves-effect waves-light btn"
                           onClick="addInput('addImageForm');">
                    <div class="row" id="addImageForm">
                        <div class="file-field input-field col offset-l3 l5">
                            <div class="btn">
                                <span>File 0</span>

                                <form:input path="images[0].multipartFile" type="file"/>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <h3>Choose ParrentRentalUnit</h3>
                        <div class="input-field col s6">
                            <input name="nameForRentalUnit" type="radio" id="testNull" value="-1" />
                            <label for="testNull">None</label><br/>
                            <c:forEach items="${allRentalUnits}" var="rent">
                                <input name="nameForRentalUnit" type="radio" id="test${rent.id}" value="${rent.id}" />
                                <label for="test${rent.id}">${rent}</label>
                            </c:forEach>



                        </div>
                    </div>


                    <form:button class="waves-effect waves-light btn-" name="Add">add</form:button>

                </form:form>
            </div>
            <script src="<c:url value="/resources/js/addInput.js"/>" language="Javascript"
                    type="text/javascript"></script>
</t:template>